import {NavStyle} from './styles'

import Logo from '../../assets/img/logotipo.png'

import { Navbar, Nav } from 'react-bootstrap'

import { useEffect, useState } from 'react'
import './style.scss'

const NavbarComponent = () => {

  const [greenHeader, setGreenHeader] = useState(false);

   useEffect(() => {
    const scrollListener = () => {
      
      if(window.scrollY > 80) {
        setGreenHeader(true);
      }else {
        setGreenHeader(false);
      }
    }

    window.addEventListener('scroll', scrollListener);

    return () => {
      window.removeEventListener('scroll', scrollListener);
    }
  });

  const estilo = {
    position: 'fixed', 
    width: "100%",
    height: "90px",
    top: '0px', 
    left: "50%", 
    transform: "translateX(-50%)",
    zIndex: "100",
    transition: "all ease 0.5s"
  }

  const estiloScroll = {
    position: 'fixed', 
    width: "100%",
    height: "77px",
    top: '0px', 
    left: "50%", 
    transform: "translateX(-50%)",
    zIndex: "100",
    backgroundColor: "rgb(226, 45, 74)",
    transition: "all ease 0.5s",
    boxShadow: "0px 5px 6px rgba(0, 0, 0, 0.22)"
  }

  // #686d73

  const estiloA = {
    zIndex: "100",
    transition: "all ease 0.2s"
  }

  const estiloAScroll = {
    boxShadow: "0px 5px 6px rgba(0, 0, 0, 0.22)",
    height: "90px",
    transition: "all ease 0.2s"
  }

  return (
    <>
      {/* <div className={greenHeader === true ? "menu-green" : ""}> */}
     
        <NavStyle id="green" style={ greenHeader ? estiloScroll : estilo } >
          <div className="container">
            <Navbar bg="light" expand="lg">
            <Navbar.Brand style={ greenHeader ? estiloAScroll : estiloA} href="#home">
              <img src={Logo} className="img-fluid" loading="lazy" alt="logo da Pargos Club" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto">
                <Nav.Link href="#sobre" className="menu-item">QUEM SOMOS</Nav.Link>
                {/* <Nav.Link href="#planos">Planos</Nav.Link> */}
                <Nav.Link href="#beneficios" className="menu-item">EVENTOS</Nav.Link>
                {/* <Nav.Link href="#informacoes">Informações</Nav.Link> */}
                <Nav.Link href="#contato" className="menu-item">CONTATO</Nav.Link>
                <Nav.Link href="#contato" className="menu-item">COMPRE JÁ</Nav.Link>
              </Nav>
            </Navbar.Collapse>
            </Navbar>
          </div>
        </NavStyle>
      {/* </div> */}
    </>
  )
}

export default NavbarComponent;
