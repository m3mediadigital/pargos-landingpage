import { Tab } from "react-bootstrap";

import {
  ContainerBonus,
  Container,
  Nav,
//   Card,
//   ContainerInfo,
//   CardInfo,
} from "./styles";

import Union from "../../assets/img/union.png";

// import Cinepolis from "../../assets/img/cinepolis.png";
// import Petz from "../../assets/img/petz.png";
// import Bk from "../../assets/img/bk.png";
// import Riachuelo from "../../assets/img/riachuelo.png";
// import img01 from "../../assets/img/img01.png";
// import img02 from "../../assets/img/img02.png";
// import img03 from "../../assets/img/img03.png";

import Carousel from 'react-grid-carousel'


const Contents = [
	{content:[
		{title: 'DAY USE'},
		{imgs: [
			{img: '/images/1.jpg'},
			{img: '/images/2.jpg'},
			{img: '/images/3.jpg'},
			{img: '/images/4.jpg'},
			{img: '/images/5.jpg'},
			{img: '/images/6.jpg'},
			{img: '/images/7.jpg'},
			{img: '/images/8.jpg'},
			{img: '/images/9.jpg'},
			{img: '/images/10.jpg'},
			{img: '/images/11.jpg'}
		]}
	]},
	{content:[
		{title: 'CASAMENTO'},
		{imgs: [
			// {img: '/images/1.jpg'},
			// {img: '/images/2.jpg'},
			// {img: '/images/3.jpg'},
			// {img: '/images/4.jpg'},
			// {img: '/images/5.jpg'},
			// {img: '/images/6.jpg'},
			// {img: '/images/7.jpg'},
			// {img: '/images/8.jpg'},
			// {img: '/images/9.jpg'},
			// {img: '/images/10.jpg'},
			// {img: '/images/11.jpg'}
		]}
	]},
  {content:[
		{title: 'ANIVERSÁRIO'},
		{imgs: [
			// {img: '/images/1.jpg'},
			// {img: '/images/2.jpg'},
			// {img: '/images/3.jpg'},
			// {img: '/images/4.jpg'},
			// {img: '/images/5.jpg'},
			// {img: '/images/6.jpg'},
			// {img: '/images/7.jpg'},
			// {img: '/images/8.jpg'},
			// {img: '/images/9.jpg'},
			// {img: '/images/10.jpg'},
			// {img: '/images/11.jpg'}
		]}
	]},
  {content:[
		{title: 'DIA DAS CRIANÇAS'},
		{imgs: [
			// {img: '/images/1.jpg'},
			// {img: '/images/2.jpg'},
			// {img: '/images/3.jpg'},
			// {img: '/images/4.jpg'},
			// {img: '/images/5.jpg'},
			// {img: '/images/6.jpg'},
			// {img: '/images/7.jpg'},
			// {img: '/images/8.jpg'},
			// {img: '/images/9.jpg'},
			// {img: '/images/10.jpg'},
			// {img: '/images/11.jpg'}
		]}
	]},
  {content:[
		{title: 'NATAL'},
		{imgs: [
			// {img: '/images/1.jpg'},
			// {img: '/images/2.jpg'},
			// {img: '/images/3.jpg'},
			// {img: '/images/4.jpg'},
			// {img: '/images/5.jpg'},
			// {img: '/images/6.jpg'},
			// {img: '/images/7.jpg'},
			// {img: '/images/8.jpg'},
			// {img: '/images/9.jpg'},
			// {img: '/images/10.jpg'},
			// {img: '/images/11.jpg'}
		]}
	]},
  {content:[
		{title: 'RÉVELLION'},
		{imgs: [
			// {img: '/images/1.jpg'},
			// {img: '/images/2.jpg'},
			// {img: '/images/3.jpg'},
			// {img: '/images/4.jpg'},
			// {img: '/images/5.jpg'},
			// {img: '/images/6.jpg'},
			// {img: '/images/7.jpg'},
			// {img: '/images/8.jpg'},
			// {img: '/images/9.jpg'},
			// {img: '/images/10.jpg'},
			// {img: '/images/11.jpg'}
		]}
	]}
]

// console.log(Contents)

const Bonus = () => {
	return (
		<Container data-aos="fade-down" id="beneficios">
			<img src={Union} alt="União" className="peixe d-none d-lg-block" />
			<ContainerBonus>
				<div className="container">
					<div className="row">
						<div className="col-12">
							<h3>Últimos Eventos</h3>
						</div>
						<Nav defaultActiveKey="0" id="nav" className="d-flex justify-content-center">
							{Contents.map((item, key) => {
								console.log(item.content[1].imgs)
								return(
									<Tab eventKey={key} key={key} title={item.content[0].title}>
										<div className="container">
											<div className="d-lg-none">
												<Carousel cols={4} rows={3} gap={2} autoplay={5000} 
													style={{ margin: '0px' }}
													loop>
													{item.content[1].imgs.map((value, key) => {
														return (
															<Carousel.Item>
																<a  data-fancybox="gallery" href={value.img}>
																	<img className="img-fluid" src={value.img} alt="Pargos Club" />
																</a>
															</Carousel.Item>
														)
													})}
												</Carousel>
											</div>
											<div className="row d-none d-lg-flex">
												{item.content[1].imgs.map((value, key) => {
													return (
														<div className="col-3 py-3">
															<a  data-fancybox="gallery" href={value.img}>
																<img className="img-fluid" src={value.img} alt="Pargos Club" />
															</a>
														</div>
													)
												})}
											</div>
										</div>
									</Tab>
								)
							})}
							{/* <Tab eventKey="fastfood" title="Fast Food">
								<div className="container">
									<div className="row cardContainer">
										<div className="col-lg-3 col-12">
											<Card>
												<img
													src={Bk}
													alt="Logo do Burger King"
													loading="lazy"
												/>
												<p>3 woppers grátis</p>
											</Card>
										</div>
									</div>
								</div>
							</Tab>
							<Tab eventKey="infantil" title="Infantil">
								<div className="container">
									<div className="row cardContainer">
										<div className="col-lg-3 col-12">
											<Card>
												<img
													src={Cinepolis}
													alt="Logo do Cinépolis"
													loading="lazy"
												/>
												<p>50% de desconto nos ingressos</p>
											</Card>
										</div>
									</div>
								</div>
							</Tab>
							<Tab eventKey="educacao" title="Educação">
								<div className="container">
									<div className="row cardContainer">
										<div className="col-lg-3 col-12">
											<Card>
												<img
													src={Riachuelo}
													alt="Logo da Riachuelo"
													loading="lazy"
												/>
												<p>15% de desconto em roupas de banho</p>
											</Card>
										</div>
									</div>
								</div>
							</Tab>
							<Tab eventKey="pets" title="Pets">
								<div className="container">
									<div className="row cardContainer">
										<div className="col-lg-3 col-12">
											<Card>
												<img src={Petz} alt="Logo do Petz" loading="lazy" />
												<p>2 banhos e tosas</p>
											</Card>
										</div>
									</div>
								</div>
							</Tab> */}
						</Nav>
					</div>
				</div>
			</ContainerBonus>
			{/* <ContainerInfo id="informacoes">
				<div className="container">
					<div className="row">
						<div className="col-12">
							<h3 style={{ paddingTop: "91px" }}>Quem Somos</h3>
						</div>
					</div>
				</div>
			</ContainerInfo>
			<div className="container" style={{ marginTop: '-13pc' }}>
				<Video>
					<iframe
						width="853"
						height="480"
						src={`https://www.youtube.com/embed/YvnAysAUHJw`}
						frameBorder="0"
						allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
						allowFullScreen
						title="Embedded youtube"
					/>
				</Video>
				<div className="row cardInfoContainer">
					<div className="col-lg-4 col-12">
						<CardInfo>
							<img src={img01} className="cardImage" alt="Casal Feliz e sorridente" loading="lazy" />
							<p>Estamos preparados para receber você e sua família com excelentes espaços</p>
						</CardInfo>
					</div>
					<div className="col-lg-4 col-12">
						<CardInfo>
							<img src={img02} className="cardImage" alt="Uma bela piscina" loading="lazy" />
							<p>Piscinas amplas, com atrações para adultos e crianças</p>
						</CardInfo>
					</div>
					<div className="col-lg-4 col-12">
						<CardInfo>
							<img src={img03} className="cardImage" alt="Drinks e itens do cardápio" loading="lazy" />
							<p>Temos um excelente cardápio para a sua alimentação</p>
						</CardInfo>
					</div>
				</div> */}
			{/* </div> */}
		</Container>
	);
};

export default Bonus;
