// import aboutImage from '../../assets/img/about.png';

import { AboutContainer } from './styles'
import Carousel from 'react-grid-carousel'
import styled from 'styled-components';

const Video = styled.div`
	overflow: hidden;
	padding-bottom: 56.25%;
	position: relative;
	height: 400px;
  	width: 100%;

	iframe {
		left: 0;
		top: 0;
		height: 100%;
		width: 100%;
		position: absolute;
	}
`
const Slider = styled.div`
	div{
		margin: 0px;
	}
`
const Imgs = [
	{img: '/images/1.jpg'},
	{img: '/images/2.jpg'},
	{img: '/images/3.jpg'},
	{img: '/images/4.jpg'},
	{img: '/images/5.jpg'},
	{img: '/images/6.jpg'},
	{img: '/images/7.jpg'},
	{img: '/images/8.jpg'},
	{img: '/images/9.jpg'},
	{img: '/images/10.jpg'},
	{img: '/images/11.jpg'},
	{img: '/images/1.jpg'}
]

const About = () => {
	return (
		<>
			<div className="container" id="sobre">
				<div className="row">
					<div className="col-lg-6 col-sm-12 d-flex justify-content-center align-items-center order-2">
						<Video data-aos="fade-right" className="d-none d-lg-block">
						<iframe
							width="853"
							height="480"
							src={`https://www.youtube.com/embed/YvnAysAUHJw`}
							frameBorder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
							allowFullScreen
							title="Embedded youtube"
						/>
						</Video>
						{/* <Img className="img-fluid" src={aboutImage} loading="lazy" alt="Imagem sobre um rio do Pargos Club" /> */}
					</div>
					<div data-aos="fade-left" className="col-lg-6 col-sm-12 order-1">
						<AboutContainer>
						<h2>Sobre o Pargos Club</h2>

						<Video className="d-lg-none pt-2 mb-4">
							<iframe
							width="853"
							height="480"
							src={`https://www.youtube.com/embed/YvnAysAUHJw`}
							frameBorder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
							allowFullScreen
							title="Embedded youtube"
							/>
						</Video>

						<p>
							Somos uma empresa com mais de 50 anos no mercado, só aqui no RN estamos a
							29 anos localizados na praia de porto mirim a 25 km de natal no litoral
							norte, contamos com uma área verde maravilhosa, 3 piscinas naturais, uma
							área de lazer completa com hospedagem e um riacho na qual o mesmo passa
							dentro do complexo, além de termos 3 atrações diferencias como skybunda,
							aerobunda, kamicaze e um parque aquático cujo o mesmo esta em
							desenvolvimento, temos pronto uma piscina infantil repleta de atrações
							para você desfrutar de todo conforto com sua família.
						</p>
						</AboutContainer>
					</div>
				</div>
			</div>
			<Slider>
				<Carousel cols={4} rows={1} gap={0} autoplay={5000} 
					style={{ margin: '0px' }}
					responsiveLayout={[
						{
							breakpoint: 768,
							cols: 2,
							rows: 1,
							gap: 10,
							loop: true,
							autoplay: 1000
						}
					]}
					loop>
					{Imgs.map((item, key) => {
						return (
							<Carousel.Item>
								<a data-fancybox="gallery" href={item.img}>
									<img data-aos="fade-up" className="img-fluid" src={item.img} alt="Pargos Club" />
								</a>
							</Carousel.Item>
						)
					})}
				</Carousel>
			</Slider>
			
		</>
	);
};

export default About;
