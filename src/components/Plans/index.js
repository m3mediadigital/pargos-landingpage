import { CardPlan } from "./styles";

import { ContainerPlan } from "./styles";

import img1 from "../../assets/img/Img1.png";
import img2 from "../../assets/img/img2.png";
import img3 from "../../assets/img/img3.png";

const Plans = () => {
  return (
    <ContainerPlan data-aos="fade-down" id="planos">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-12 pr-lg-0">
            <img
              src={img3}
              className="first-img"
              alt="imagem de uma mulher em uma lagoa do Pargos Club"
              loading="lazy"
            />
          </div>
          <div className="col-lg-4 col-12 pl-lg-0">
            <img
              src={img1}
              className="img-fluid second-img"
              alt="imagem de uma mulher em uma lagoa do Pargos Club"
              loading="lazy"
            />
          </div>
          <div className="col-lg-8 col-12 pr-lg-0">
            <CardPlan>
              <div className="content">
                <h3>O melhor para você!</h3>
                <p className="">
                  Pargos Club é o local ideal para quem ama diversão e busca momentos inesquecíveis em família.
                  Em um único local, você encontra a combinação perfeita entre aventuras e descanso.
                  Passeios emocionantes como:
                  Skibunda, Kamikaze, Aerobunda e o contato com a natureza através das trilhas ecológicas, lagos e animais silvestres. Uma experiência com a natureza que acalma qualquer alma!
                  Atrações para criançada, como piscina infantil e playground para momentos de pura diversão.
                  Acomodações aconchegantes, além do acesso a ampla área de lazer com piscinas, brinquedos e restaurante.
                  03 incríveis unidades, localizadas em Porto Mirim/RN, Salinopolis/PA e Icaraí/CE.
                </p>
                <div className="containerButtons">
                  {/* <a href="#formulario" className="cta-left"> */}
                  <a href="#contato" className="cta-left">
                    Faça o seu Evento conosco
                  </a>
                  {/* <a href="#beneficios" className="cta-right">
                    Benefícios
                  </a> */}
                </div>
              </div>
            </CardPlan>
          </div>
          <div className="col-lg-4 col-12  pl-lg-0">
            <img
              src={img2}
              className="img-fluid third-img"
              alt="imagem de uma mulher em uma lagoa do Pargos Club"
              loading="lazy"
            />
          </div>
        </div>
      </div>
    </ContainerPlan>
  );
};

export default Plans;
