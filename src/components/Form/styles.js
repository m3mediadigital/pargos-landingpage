import Styled, { css } from 'styled-components';

export const FormStyle = Styled.form`
  h3{
    font-size: 45px;
    font-weight: normal;
    font-family: Raleway;
    color: #E22D4A;
    letter-spacing: 1.35px;
    margin-top: 53px;
    margin-bottom: 35px;
  }
  .button{
    @media screen and (max-width: 960px) {
      button{
        padding-left: 60px;
        padding-right: 60px;
      }
    }
  }
`;

export const ContainerForm = Styled.form`
  .input-data { 
    margin-top: 40px;

    height: 40px;
    width: 100%;
    position: relative;
    font-size: 14px;

    input {
      height: 100%;
      width: 100%;
      border: none;
      border-bottom: 3px solid #BEBEBE;
      font-size: 14px;
      color: #6F6F6F;
      
      &:focus{
        outline: none;
        border-bottom: 3px solid #E22D4A;
      }

      &:focus ~ label {
        transform: translateY(-20px);
        font-size: 12px;
        color: #666666;
      }

      &:valid ~ label{
        transform: translateY(-20px);
        font-size: 12px;
        color: black;
      }
    }
   
    label {
      text-transform: uppercase;

      position: absolute;
      bottom: 0px;
      left: 0;
      color: #6F6F6F;
      pointer-events: none;
      transition: all 0.3s ease;
      font-size: 14px;
    }
  }

  div.input-select{
    margin-top: 48px;
    font-size: 14px;

    .select__control--menu-is-open{
      border-bottom: 3px solid #E22D4A !important;
    }

    div.select__control{
      box-shadow: none;

      background: transparent;
      border: none;
      margin: 0;
      padding: 0;
      border-bottom: 3px solid #BEBEBE;
      border-radius: 0;
      min-height: 0;
      transition: all 0.2s ease;

      div.select__value-container{
        padding: 0;
        margin: 0;

        div.select__placeholder{
          background: transparent;
          color: #6F6F6F;
          
          bottom: -11px;
        }
      }

      div.select__indicators{
        .select__indicator{
          padding: 0px !important;
        }

        span.select__indicator-separator{
          display: none;
        }
      }
    }
  }
`;


export const ContainerModal = Styled.div`
  position: fixed;
  top: 0;
  left: 0;

  visibility: hidden;

  /* display: flex; */
  /* justify-content: center; */
  /* align-items: center; */

  width: 100%;
  height: 100%;

  background: #0059A4;
  z-index: 9999;

  transition: all 0.3s ease 0s;
  /* cursor: pointer; */
  opacity: 0;

  // overflow-y: scroll;
  overflow: scroll;

  color: white;

  ${(props) =>
    props.isOpen &&
    css`
      visibility: visible;
      opacity: 1;

      & > div {
        transform: translateY(0);
        opacity: 1;
      }
    `}

`;

export const ContentPlan = Styled.div`
  background-color: ${ props => (props.color === 0  ? 'orange' : props.color === 1 ? '#ADD54E' : props.color === 2 ? 'red' : 'blue') };
  display: flex;
  flex-direction: column;
  align-items: center;
  // justify-content: space-between;
  min-height: 550px;
  @media {

  }
  
  color: white;
  padding: 3vh;
  text-align: justify;
  border-radius: 1vh;
  margin-top: 1vh;
  .plano-conteudo {
    margin-top: 3vh;
  }
  .plano{
    &-nome {
      width: 170px;
      text-align: center;
      
      
      
    }
    &-btn {
      width: 140px;
      // align-items: center;
      background-color: white;
      color: #000;
      padding: 0 0 0 0;
      height: 50px;
      
    }
  }
  p {
    // border: solid 1px green;
    // width: 20px;
    min-height: 200px;
    // max-height: 800px;
  }
`;