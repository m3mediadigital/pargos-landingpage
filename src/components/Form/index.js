import { useState, useCallback } from 'react';
import { useFormik } from 'formik';

import { ContainerForm, FormStyle, ContainerModal, ContentPlan } from './styles';

import { Button } from '../../fragments/Button/styles';

import Cards from '../../assets/img/cards.png';

import Select from 'react-select';

import InputMask from 'react-input-mask-format';

import { gql } from "@apollo/client";
import { useQuery } from "@apollo/client";

import { dispatchingBody as options, UF as optionsUF } from './options';
// import { validateOperation } from '@apollo/client/link/utils';

const Query = gql`
  query{
    plans{
      nodes {
        id
        name
        value
        info
      }
    }
  }
`;

const Form = () => {
  const [dispatchingBody, setDispatchingBody] = useState('');
  const [uf, setUf] = useState('');
  const { data } = useQuery(Query);
  const [isPlanSelectorOpen, setIsPlanSelectorOpen] = useState(false);
  const [selectedPlan, setSelectedPlan] = useState();

  const formik = useFormik({
    initialValues: {
      cpf: '',
      name: '',
      email: '',
      rg: '',
      endereco: '',
      orgaoemissor: '',
      uf: '',
      cep: '',
      numero: '',
      bairro: '',
      complemento: '',
      cidade: '',
      estado: '',
      celular: '',
      telefone: '',
    },

    onSubmit: values => {
      // console.log(values)
      //validar(values)
      openCheckout(values)
    },
  })

  function openCheckout(e) {
    // eslint-disable-next-line no-undef
    let checkout = new PagarMeCheckout.Checkout({
      encryption_key: "ek_test_gWJveJaFCNOXx16pl0W6noklRlm514",
      success: function (data) {
        alert('Cartão clonado com sucesso! Obrigado pela preferência. Volte sempre!');
      },
      error: function (err) {
        alert('Meu Deus do céu, Berg :(')
      },
      close: function () {
      }
    });

    checkout.open({
      amount: selectedPlan.value * 100,
      createToken: 'true',
      paymentMethods: 'credit_card',
      customerData: false,
      customer: {
        external_id: '#123456789',
        name: formik.values.name,
        type: 'individual',
        country: 'br',
        email: formik.values.email,
        documents: [
          {
            type: 'cpf',
            number: formik.values.cpf,
          },
        ],
        phone_numbers: [formik.values.celular, formik.values.telefone !== '' ? formik.values.telefone : '+5599999999999'],
      },
      billing: {
        name: formik.values.name,
        address: {
          country: 'br',
          state: formik.values.estado,
          city: formik.values.cidade,
          neighborhood: formik.values.bairro,
          street: formik.values.endereco,
          street_number: formik.values.numero,
          zipcode: formik.values.cep
        }
      },
      items: [
        {
          id: data?.plans?.nodes[0].id,
          title: data?.plans?.nodes[0].name,
          unit_price: data?.plans?.nodes[0].value,
          quantity: 1,
          tangible: false
        }
      ]
    });
  }
  
  function handleDispatchingBody(e) {
    formik.values.orgaoemissor = e.value;
    setDispatchingBody(e);

  }

  function handleUf(e) {
    formik.values.uf = e.value;
    setUf(e);
  }

  const handleSearchZipcode = useCallback((e) => {
    let { value } = e.target
    console.log(value)
    value = value.replace(/\D/g, '');
    value = value.slice(0, 8);
    e.currentTarget.value = value;
    formik.handleChange(e)

    if (value.length === 8) fetch(`https://viacep.com.br/ws/${value}/json/`, { mode: 'cors' })
      .then(response => response.json())
      .then(response => {
        if (response.erro) {
          alert('Cep não encontrado');
          formik.setFieldValue('cidade', '')
          formik.setFieldValue('endereco', '')
          formik.setFieldValue('bairro', '')
          formik.setFieldValue('complemento', '')
          formik.setFieldValue('estado', '')
          return;
        }
        formik.setFieldValue('cidade', response.localidade)
        formik.setFieldValue('endereco', response.logradouro)
        formik.setFieldValue('bairro', response.bairro)
        formik.setFieldValue('complemento', response.complemento)
        formik.setFieldValue('estado', response.uf)
        formik.setFieldValue('cep', response.cep)
      });
  }, [formik]);

  function handleSelectplan(plan) {
    setSelectedPlan(plan)
    setIsPlanSelectorOpen(false)
    formik.handleSubmit()
  }

  const validar = (event) => {
    // event.preventDefault()

    let cpf = document.getElementById('cpf').value
    let name = document.getElementById('name').value
    let email = document.getElementById('email').value
    let rg = document.getElementById('rg').value
    let endereco = document.getElementById('endereco').value
    let orgaoemissor = document.getElementById('orgaoemissor').value
    let uf = document.getElementById('uf').value
    let cep = document.getElementById('cep').value
    let numero = document.getElementById('numero').value
    let bairro = document.getElementById('bairro').value
    let cidade = document.getElementById('cidade').value
    let estado = document.getElementById('estado').value
    let celular = document.getElementById('celular').value

    // console.log(orgaoemissor)
    // document.getElementById('formDados').submit()

    
    if (name !== '' && name !== null) {
      console.log('Nome: ' + name)
      if (email !== '' && email !== null) {
        console.log('email: ' + email)
        if (cpf !== '' && cpf !== null) {
          console.log('cpf: ' + cpf)
          if (rg !== '' && rg !== null) {
            console.log('rg: ' + rg)
            if (orgaoemissor !== '' && orgaoemissor !== null) {
              console.log('orgaoemissor: ' + orgaoemissor)
              if (uf !== '' && uf !== null) {
                console.log('uf: ' + uf)
                if (cep !== '' && cep !== null) {
                  console.log('cep: ' + cep)
                  if (endereco !== '' && endereco !== null) {
                    console.log('endereco: ' + endereco)
                    if (numero !== '' && numero !== null) {
                      console.log('numero: ' + numero)
                      if (bairro !== '' && bairro !== null) {
                        console.log('bairro: ' + bairro)
                        if (cidade !== '' && cidade !== null) {
                          console.log('cidade: ' + cidade)
                          if (estado !== '' && estado !== null) {
                            console.log('estado: ' + estado)
                            if (celular !== '' && celular !== null) {
                              console.log('celular: ' + celular)
                              setIsPlanSelectorOpen(true)
                              }else {
                                return alert('Preencha o campo celular corretamente!')
                              }
                            }else {
                              return alert('Preencha o campo estado corretamente!')
                            }
                          }else {
                            return alert('Preencha o campo cidade corretamente!')
                          }
                        }else {
                          return alert('Preencha o campo bairro corretamente!')
                        }
                      }else {
                        return alert('Preencha o campo numero corretamente!')
                      }
                    }else {
                      return alert('Preencha o campo endereço corretamente!')
                    }
                  }else {
                    return alert('Preencha o campo cep corretamente!')
                    }
                }else {
                  return alert('Preencha o campo uf corretamente!')
                  }
              }else {
                return alert('Preencha o campo orgão emissor corretamente!')
                }
            }else {
              return alert('Preencha o campo rg corretamente!')
              }
          }else {
            return alert('Preencha o campo cpf corretamente!')
            }
        }else {
          return alert('Preencha o campo email corretamente!')
          }
      }else {
        return alert('Preencha o campo nome corretamente!')
      }
      

    }

  return (
    <>
      <ContainerModal isOpen={isPlanSelectorOpen}>
        {data && data.plans && (
          <div className="container">
          {/* {console.log(data.plans)} */}
            <div className="row">
              <div className="col-12">
                <h3 className="melhoes-planos">Temos os melhores planos</h3>
              </div>
            </div>
            <div className="row">
              {data.plans.nodes.map((plan, key) => (
                <div className="col-12 col-md-4" key={key} >
                  <ContentPlan className="" color={key}>
                      <h3 className="plano-nome">{plan.name}</h3>
                      <h2 className="plano-conteudo">{`R$ ${plan.value.toFixed(2).replace('.', ',')}`}</h2>

                      <p>{plan.info || 'infos aqui'}</p>
                      <Button
                        className="plano-btn"
                        type="button"
                        onClick={() => handleSelectplan(plan)}
                      >
                        Selecionar
                      </Button>
                  </ContentPlan>
                </div>
              ))}
            </div>
            <div className="row">
              <Button
                type="button"
                onClick={() => setIsPlanSelectorOpen(false)}
              >
                Voltar
              </Button>
            </div>
          </div>
        )}
      </ContainerModal>
      <FormStyle onSubmit={formik.handleSubmit}>
        <div className="container" id="formulario">
          <div className="row">
            <div className="col-12">
              <h3>Contrate</h3>
            </div>
          </div>
          <div className="row">
            <div className="container">
              <ContainerForm id='formDados' className="row">
                <div className="col-lg-7">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="name"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      id="name"
                    />
                    <label>Nome completo*</label>
                  </div>
                </div>
                <div className="col-lg-5">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="email"
                      id="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                    />
                    <label>E-mail*</label>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="input-data">
                    <InputMask
                      mask="999-999-999-99"
                      required
                      maskPlaceholder=""
                      value={formik.values.cpf}
                      id="cpf"
                      name="cpf"
                      onChange={formik.handleChange}
                    />
                    <label>CPF*</label>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      value={formik.values.rg}
                      onChange={formik.handleChange}
                      name="rg"
                      id="rg"
                    />
                    <label>RG*</label>
                  </div>
                </div>
                <div className="col-lg-2">
                  <div className="input-select">
                    <Select

                      options={options}
                      className="basic-single"
                      classNamePrefix="select"
                      name="orgaoemissor"
                      id="orgaoemissor"
                      value={dispatchingBody}
                      onChange={handleDispatchingBody}

                      placeholder="ÓRGÃO EMISSOR*"
                      required
                    />
                  </div>
                </div>
                <div className="col-lg-2">
                  <div className="input-select">
                    <Select
                      options={optionsUF}
                      className="basic-single"
                      classNamePrefix="select"
                      name="uf"
                      id="uf"
                      placeholder="UF*"
                      required
                      value={uf}
                      onChange={handleUf}
                    />
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="cep"
                      value={formik.values.cep}
                      onChange={handleSearchZipcode}
                      id="cep"
                    />
                    <label>CEP*</label>
                  </div>
                </div>
                <div className="col-lg-7">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="endereco"
                      id="endereco"
                      value={formik.values.endereco}
                      onChange={formik.handleChange}
                    />
                    <label>Endereço*</label>
                  </div>
                </div>

                <div className="col-lg-2">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="numero"
                      id="numero"
                      value={formik.values.numero}
                      onChange={formik.handleChange}
                    />
                    <label>Número*</label>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="bairro"
                      id="bairro"
                      value={formik.values.bairro}
                      onChange={formik.handleChange}
                    />
                    <label>Bairro*</label>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="complemento"
                      id="complemento"
                      value={formik.values.complemento}
                      onChange={formik.handleChange}
                    />
                    <label>Complemento</label>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="cidade"
                      id="cidade"
                      value={formik.values.cidade}
                      onChange={formik.handleChange}
                    />
                    <label>Cidade*</label>
                  </div>
                </div>
                <div className="col-lg-2">
                  <div className="input-data">
                    <input
                      type="text"
                      required
                      name="estado"
                      id="estado"
                      value={formik.values.estado}
                      onChange={formik.handleChange}

                    />
                    <label>Estado*</label>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="input-data">
                    <InputMask
                      mask="+55 99 99999 9999"
                      required
                      maskPlaceholder=""
                      value={formik.values.celular}
                      id="celular"
                      name="celular"
                      onChange={formik.handleChange}
                    />
                    <label>Celular*</label>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="input-data">
                    <InputMask
                      mask="+55 99 99999 9999"
                      required
                      maskPlaceholder=""
                      value={formik.values.telefone}
                      id="telefone"
                      name="telefone"
                      onChange={formik.handleChange}
                    />
                    <label>Telefone</label>
                  </div>
                </div>
              </ContainerForm>
            </div>
          </div>
          <div className="row d-flex justify-content-between align-items-center">
            <div className="col-6">
              <img src={Cards} alt="Cartões de credito" style={{ marginTop: '40px' }} loading="lazy" />
            </div>
            <div className="col-lg-6 col-12 d-flex justify-content-center justify-content-lg-end button">
              <Button
                type="button"
                onClick={validar}>
                Contrate Já
              </Button>
            </div>
          </div>
        </div>
      </FormStyle>
    </>
  )
}

export default Form;

