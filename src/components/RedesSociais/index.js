import React from 'react';
import './style.css';

const RedesSociais = ()=> {
  return (
    <section className="redes m-0 ">
      <ul className=" pt-3 list-unstyled d-flex justify-content-around flex-wrap">
        <li className="col-12 col-lg-4 d-flex align-items-center justify-content-center py-2">
          <p href="" className="" style={{ color: "#7b7676" }} >
            {/* <img style={{ width: "20px" }} src="images/logo-m3.svg" alt="Nova M3" /> */}
            2021 - Todos os direitos reservados. 50 anos no mercado
          </p>
        </li>
        <li className="col-12 col-lg-4 d-flex align-items-center justify-content-center py-2">
          <a href="https://novam3.com.br/" target="_blank" rel="noreferrer" className="" style={{ color: "#7b7676" }} >
            <img style={{ width: "20px", margin: "2px" }} src="images/logo-m3.svg" alt="Nova M3" />
            nova<strong>m3</strong>
          </a>
        </li>
        <li className="col-12 col-lg-4 py-lg-2">
          <ul className="list-unstyled d-flex align-items-center justify-content-center">
            <li className="icon-instagram px-2">
              <a className="icon-instagram" href="https://www.instagram.com/pargosclub/" target="_blank" rel="noreferrer"><i style={{ fontSize: "30px", color: "#7b7676" }} class="fab fa-instagram"></i></a>
            </li>
            <li className=" px-2">
              <a className="icon-youtube" href="https://www.youtube.com/channel/UCLg0qEFA5SgXuk_NWDM0LoA" target="_blank" rel="noreferrer"><i style={{ fontSize: "30px", color: "#7b7676" }} class="fab fa-youtube"></i></a>
            </li>
          </ul>
        </li>
      </ul>
    </section>
  )
}

export default RedesSociais;