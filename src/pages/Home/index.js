import React from 'react'
import Navbar from '../../components/Navbar';

import { BannerContainer, App } from './styles';
import About from '../../components/About';
// import Associated from '../../components/Associated';
import Plans from '../../components/Plans';
// import Form from '../../components/Form';
import Bonus from '../../components/bonus';
import Maps from '../../components/Maps';
import Contact from '../../components/contact';
import Zap from '../../assets/img/whatsappicon.png';
import RedesSociais from '../../components/RedesSociais';

export default function Home() {
  const ref = React.useRef(null)
  return (
    <App>
      <a href="https://api.whatsapp.com/send?phone=5584998947268&text=Ol%C3%A1%2C%20Pargos%20Club!%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es" className="zap">
        <img src={Zap} alt="Imagem do Whatsapp" />
      </a>
      <Navbar />
      <BannerContainer data-aos="fade-down"
        onClick={
          () => ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' })
        }
      />
      <About />
      {/* <Associated /> */}
      <Plans />
      {/* <Contact /> */}
      {/* <div ref={ref}>
        <Form />
      </div> */}
      <Bonus />
      <Contact />
      <Maps />
      <RedesSociais />

      {/* <Contact /> */}


    </App>
  )
};